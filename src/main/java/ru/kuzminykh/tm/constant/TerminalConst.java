package ru.kuzminykh.tm.constant;

public class TerminalConst {
    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String EXIT = "exit";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW_INDEX = "project-view-index";
    public static final String PROJECT_VIEW_ID = "project-view-id";
    public static final String PROJECT_REMOVE_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_NAME = "project-remove-by-name";
    public static final String PROJECT_REMOVE_INDEX = "project-remove-by-index";
    public static final String PROJECT_UPDATE_INDEX = "project-update-by-index";
    public static final String PROJECT_UPDATE_ID = "project-update-by-id";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW_INDEX = "task-view-index";
    public static final String TASK_VIEW_ID = "task-view-id";
    public static final String TASK_REMOVE_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_INDEX = "task-remove-by-index";
    public static final String TASK_UPDATE_INDEX = "task-update-by-index";
    public static final String TASK_UPDATE_ID = "task-update-by-id";

}
